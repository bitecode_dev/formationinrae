# Ceci est du markdown

## Ceci est un sous-titre

- point 1 et du *italic* et **gras**
- point 2 et `code inline`

Liste ordonnées : 

1. fdsq
1.  fdsqf 
1. fdsq

$$
f(x) = x^2 
$$

Ceci n'est pas une pipe: $f(x) = x^2$ mais une equation

Du code Python:

```python 
for x in range(10):
    print(x)
```

Un lien inline: <https://www.example.com> mais on peut aussi le [mettre dans un texte](https://www.example.com).

Une citation:

> il a dit
 

(ref1)=
## Autre sous titre 


Aller voir la section: {ref}`ceci va vers le titre du dessus<ref1>`
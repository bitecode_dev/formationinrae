def task_build_public_doc():
    return {"actions": ["sphinx-build doc/source public"]}


def task_add_push():
    return {
        "actions": [
            "git add .",
            'git commit -m "Ceci est un test"',
            "git push origin main",
        ],
        "verbosity": 2,
    }
